﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SiMay.RemoteControls.Core
{
    public interface IScheduleTrigger
    {
        void Execute();
    }
}
